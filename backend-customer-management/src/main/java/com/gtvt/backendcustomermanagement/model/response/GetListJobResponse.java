package com.gtvt.backendcustomermanagement.model.response;

import lombok.Data;

@Data
public class GetListJobResponse {
    private String jobName;
    private String employeeUsedCount;
}

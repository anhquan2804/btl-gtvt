package com.gtvt.backendcustomermanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendCustomerManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendCustomerManagementApplication.class, args);
	}

}
